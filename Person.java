import java.util.Random;


public class Person {

	private String name;
	private String gender;

	public Person() {
		String[] genders = { "Male", "Female" };
		String[] names = {"|Vaso","|Galq","|Blabo","|Lalo","|Sami Aladin","|ObiOne","|Ivet","|Baba mi|","|Sexy Lady","|Kasandra","|Angi","|Osvaldita","|Osvaldo"};
		setName(names[new Random().nextInt(3) + 1]);
		
		setGender(genders[new Random().nextInt(2) + 1]);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

}
